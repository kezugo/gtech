var liveServer = require("live-server");

var params = {
  open: false,
  root: "client", // Set root directory that's being server.
  wait: 100 // Waits for all changes, before reloading.
};
liveServer.start(params);
