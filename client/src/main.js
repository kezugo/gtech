/**
 * Application entry point
 */

(function(ng) {
  'use strict';

  // Main application module dependencies definition and configuration
  ng
    .module('gtech', [
      'ui.router',
      'angularUtils.directives.uiBreadcrumbs',
      'gtech.model',
      'gtech.category',
      'gtech.product',
      'gtech.cart'
    ])

    .config(function($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/'); // if url is unknown redirect to home page
      $stateProvider

        .state('home', { // state represents app entry point
          "url": "/",
          displayName: 'Home',
          "views": {
            "header": {
              templateUrl: 'src/layout/header.html',
              controller: 'HeaderController'
            },
            "footer": {
              templateUrl: 'src/layout/footer.html'
            },
            "main": {
              controller: 'MainController'
            }
          }
        })

        .state('home.category', { // filter out the product list by category
          "url": "/category/:id",
          displayName: '{{category.name}}',
          resolve: {
            category: function($stateParams, products) {
              return products.getCategoryById($stateParams.id);
            }
          },
          views: {
            "main@": {
              template: '<products-list flex category-id="{{categoryId}}"></products-list>',
              controller: function($scope, $stateParams) {
                $scope.categoryId = $stateParams.id;
              }
            }
          }
        })

        .state('home.product', { // shows particular product
          "url": "product/:id",
          // @todo prepare product name as display name
          displayName: 'Product'
        })

        .state('home.cart', { // shows the cart
          "url": "cart",
          displayName: 'Cart',
          views: {
            "main@": {
              template: '<cart></cart>'
            }
          }
        })

        .state('home.cart.checkout', { // shows the checkout process page
          "url": "cart/checkout",
          displayName: 'Checkout',
          views: {
            "main@": {
              template: '<cart-checkout></cart-checkout>'
            }
          }
        });

    })
    .run();

})(angular);
