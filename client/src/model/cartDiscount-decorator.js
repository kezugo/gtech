/**
 * Cart Service
 **/

(function(ng, module, _) {
  'use strict';

  module
    .config(function($provide){
      $provide.decorator('cart', function($delegate){
        $delegate.getDiscountTotalPrice = function() {
          var total = $delegate.getTotalPrice();
          return (total > 50 ) ? (total * 0.9) : total;
        };
        return $delegate;
      });
    });

})(angular, angular.module('gtech.model'), _);

