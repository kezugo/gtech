/**
 * Product Service
 **/

(function(ng, module) {
  'use strict';

  module
    .service('products', function(productsApi) {

      var that = this;

      this.getCategories = function() {
        return productsApi.then(function(products) {
          var counter = 2;
          return _.union([{
            id: 1,
            name: 'All Products'
          }], _.map(_.unique(products, function(product) {
            return product.category;
          }), function(product) {
            return {
              name: product.category,
              id: counter++
            }
          }));
        });
      };

      this.getCategoryById = function(id) {
        return that.getCategories().then(function(categories) {
          return _.find(categories, function(category) {
            return category.id == id;
          });
        });
      };

      this.getProductsByCategoryId = function(id) {
        if(id == 1) {
          return productsApi.then(function(products){
            return products;
          });
        }
        return that.getCategoryById(id).then(function(category) {
          return productsApi.then(function(products) {
            return _.filter(products, 'category', category.name);
          });
        })
      };

      this.getProductById = function(id) {
        return productsApi.then(function(products) {
          return _.find(products, 'id', id);
        });
      };

    });

})
(angular, angular.module('gtech.model'));

