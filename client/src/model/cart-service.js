/**
 * Cart Service
 **/

(function(ng, module, _) {
  'use strict';

  module
    .service('cart', function() {

      var products = [];

      this.getProducts = function() {
        return products;
      };

      this.addToCart = function(product, quantity) {
        var addedProduct = _.find(products, 'id', product.id);
        if(!addedProduct) {
          addedProduct = _.defaults(product);
          addedProduct.quantity = 0;
          products.push(addedProduct);
        }
        addedProduct.quantity += (quantity || 1);
      };

      this.removeFromCart = function(productId, quantity) {
        var itemIndex = _.findIndex(products, 'id', productId);
        if(products[itemIndex].quantity === 1) {
          products.splice(itemIndex, 1);
        } else {
          products[itemIndex].quantity -= (quantity || 1);
        }
      };

      this.getTotalPrice = function() {
        var total = 0;
        _.each(products, function(item){  // pluck
          total += item.price * item.quantity;
        });
        return total;
      };

      this.placeOrder = function() {
        products = [];
      };

    });

})(angular, angular.module('gtech.model'), _);

