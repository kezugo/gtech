/**
 * Products Data Transfer Object Service
 **/

(function(ng, module) {
  'use strict';

  var data = [
    {
      "category": "Men Accessories",
      "id": 1,
      "image": "jacket.png",
      "name": "Winter Jacket",
      "price": 20
    },
    {
      "category": "Men Accessories",
      "id": 2,
      "image": "watch.png",
      "name": "Wrist Watch",
      "price": 30
    },
    {
      "category": "Women Accessories",
      "id": 3,
      "image": "band.png",
      "name": "Wrist Band",
      "price": 5
    },
    {
      "category": "Women Accessories",
      "id": 4,
      "image": "bag.png",
      "name": "Hand bag",
      "price": 20
    },
    {
      "category": "Children Accessories",
      "id": 5,
      "image": "shoes.png",
      "name": "Shoes",
      "price": 20
    },
    {
      "category": "Children Accessories",
      "id": 6,
      "image": "scraf.png",
      "name": "Kids Scraf",
      "price": 10
    },
    {
      "category": "Sports Accessories",
      "id": 7,
      "image": "soccerball.png",
      "name": "Soccer Ball",
      "price": 20
    },
    {
      "category": "Sports Accessories",
      "id": 8,
      "image": "baseball.png",
      "name": "Base Ball",
      "price": 15
    }
  ];

  module
    .factory('productsApi', function($q, $http) {
      var deferred = $q.defer();
      var flickrUrl = '//flickrbrowser.herokuapp.com/pics?tags=';
      var flickrPromise;

      _.map(data, function(product) {
        flickrPromise = $http.get(flickrUrl + product.name.split(" ").join(','))
          .then(function(images) {
            if (images.data.items[0]) {
              product.image = images.data.items[0].media.m;
            } else {
              // @todo image finding fallback
            }
          });
      });

      flickrPromise.then(function() {
        deferred.resolve(data);
      });

      return deferred.promise;
    });

})(angular, angular.module('gtech.model'));

