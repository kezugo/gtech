/**
 * Header Controller file
 */

(function(ng, module) {
  'use strict';

  module
    .controller('HeaderController', function($scope, cart) {
      $scope.$watch(function() {
        return cart.getProducts().length !== $scope.itemsCountInCart;
      }, function(newV, oldV) {
        $scope.itemsCountInCart = cart.getProducts().length;
      });
    });

})(angular, angular.module('gtech'));
