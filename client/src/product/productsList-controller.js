/**
 * Products List Controller
 **/

(function(ng, module) {
  'use strict';

  module
    .controller('productsList', function($scope, products, cart) {
      $scope.products = [];
      $scope.$watch('categoryId', function(categoryId) {
        if (categoryId) {
          products.getProductsByCategoryId(categoryId).then(function(products) {
            $scope.products = products;
          });
        }
      });

      $scope.addToCart = function(productId) {
        products.getProductById(productId).then(function(product){
          cart.addToCart(product);
        });
      };

    });

})(angular, angular.module('gtech.product'));

