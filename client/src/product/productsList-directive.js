/**
 * Products List Directive
 **/

(function(ng, module) {
  'use strict';

  module
    .directive('productsList', function() {
      return {
        scope: {
          'categoryId': '@'
        },
        controller: 'productsList',
        templateUrl: 'src/product/products-list.html',
        link: function(scope, el, attrs) {}
      }
    });

})(angular, angular.module('gtech.product'));

