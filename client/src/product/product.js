/**
 * Product module definition
 */

(function(ng) {
  'use strict';

  ng
    .module('gtech.product', [
      'gtech.cart',
      'gtech.model'
    ]);

})(angular);
