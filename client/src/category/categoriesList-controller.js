/**
 * Categories List Controller
 **/

(function(ng, module) {
  'use strict';

  module
    .controller('categoriesList', function($scope, products) {
      var ctrl = this;
      products.getCategories().then(function(categories){
        ctrl.categories = categories;
      });
    });

})(angular, angular.module('gtech.category'));

