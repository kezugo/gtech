/**
 * Categories List Directive
 **/

(function(ng, module) {
  'use strict';

  module
    .directive('categoriesList', function() {
      return {
        scope: {},
        controller: 'categoriesList',
        controllerAs: 'ctrl',
        bindToController: true, // use angular 1.3 bindToController syntax
        templateUrl: 'src/category/categories-list.html',
        link: function(scope, el, attrs) {}
      }
    });

})(angular, angular.module('gtech.category'));

