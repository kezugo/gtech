/**
 * Category module definition
 */

(function(ng) {
  'use strict';

  ng
    .module('gtech.category', [
      'gtech.model'
    ]);

})(angular);
