/**
 * Cart Controller
 **/

(function(ng, module) {
  'use strict';

  module
    .controller('cart', function($scope, cart) {
      $scope.products = cart.getProducts();
      $scope.removeFromCart = function(productId){
        cart.removeFromCart(productId);
      }
    });

})(angular, angular.module('gtech.cart'));

