/**
 * Cart module definition
 */

(function(ng) {
  'use strict';

  ng
    .module('gtech.cart', [
      'gtech.product',
      'gtech.model'
    ]);

})(angular);
