/**
 * Cart Checkout Directive
 **/

(function(ng, module) {
  'use strict';

  module
    .directive('cartCheckout', function() {
      return {
        scope: {},
        controller: 'cartCheckout',
        templateUrl: 'src/cart/cart-checkout.html',
        link: function(scope, el, attrs) {}
      }
    });

})(angular, angular.module('gtech.cart'));

