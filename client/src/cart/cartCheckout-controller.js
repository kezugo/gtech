/**
 * Cart Checkout Controller
 **/

(function(ng, module) {
  'use strict';

  module
    .controller('cartCheckout', function($scope, cart) {
      $scope.products = cart.getProducts();
      $scope.totalPriceAfterDiscount = cart.getDiscountTotalPrice();
      $scope.totalPrice = cart.getTotalPrice();
    });

})(angular, angular.module('gtech.cart'));

