/**
 * Cart Directive
 **/

(function(ng, module) {
  'use strict';

  module
    .directive('cart', function() {
      return {
        scope: {},
        controller: 'cart',
        templateUrl: 'src/cart/cart.html',
        link: function(scope, el, attrs) {}
      }
    });

})(angular, angular.module('gtech.cart'));

