### GTech Recruitment - Maciej Guzek

#### Installation

Clone the git repository and then run npm install in the folder where cloned. After installaion
you can run the http server by running *node ./bin/live-server.js* in the root folder of application.

#### Informations

The application has been done with angular 1.4. Bower is the package manager for web. The structure is 
quite typical. I've used Angular Material Design module for design. TBH, I did it first time :) I want to
learn something new from this task.

#### Improvements

I'll try to update this section asap and improve the app in the next couple of days. Application definietly
suffers from lack of tests (unit tests and e2e tests). I didn't have enough time to prepare such ones.
Also RWD would be a great feature, because of use angular material design - I didn't prepare responsive
website. Also, again because lack of time, application hasn't been prepared with sass or any css preprocessor.
The gulp buildfile is not created because such simple structure doesn't need build script. Of course to
deploy and build the application it is necessary to create build script (I'll do it as a gulp)

